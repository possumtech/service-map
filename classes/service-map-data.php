<?php

class Service_Map_Data {

	public static function do_tables() {

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();

		$sites = <<<SITES

CREATE TABLE {$wpdb->prefix}service_map_sites (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	modified TIMESTAMP,
	status CHAR(10) DEFAULT 'ACTIVE',
	label NVARCHAR(100) NOT NULL,
	street NVARCHAR(100) NOT NULL,
	city NVARCHAR(100) NOT NULL,
	state NVARCHAR(100) NOT NULL,
	zip CHAR(5) NOT NULL,
	lat FLOAT (10,6),
	lng FLOAT (10,6),
	UNIQUE KEY id (id)
	) {$charset_collate};

SITES;

		dbDelta( $sites );

		$assignments = <<<ASSIGNMENTS

CREATE TABLE {$wpdb->prefix}service_map_assignments (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	assigned_to mediumint(9) NOT NULL,
	assigned_by mediumint(9) NOT NULL,
	site mediumint(9) NOT NULL,
	status CHAR(10) DEFAULT 'ACTIVE',
	schedule DATETIME,
	modified TIMESTAMP,
	original DATETIME,
	UNIQUE KEY id (id)
	) {$charset_collate};

ASSIGNMENTS;

		dbDelta( $assignments );

		$notes = <<<NOTES

CREATE TABLE {$wpdb->prefix}service_map_notes (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	author mediumint(9) NOT NULL,
	site mediumint(9) NOT NULL,
	status CHAR(10) DEFAULT 'ACTIVE',
	note TEXT,
	modified TIMESTAMP,
	original DATETIME,
	UNIQUE KEY id (id)
	) {$charset_collate};

NOTES;

		dbDelta( $notes );

		$events = <<<EVENTS

CREATE TABLE {$wpdb->prefix}service_map_events (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	author mediumint(9) NOT NULL,
	site mediumint(9) NOT NULL,
	event CHAR(10),
	modified TIMESTAMP,
	original DATETIME,
	UNIQUE KEY id (id)
	) {$charset_collate};

EVENTS;

		dbDelta( $events );

	}

	public static function get_sites(
		$offset = 0,
		$limit = 99,
		$lat_min =  -90.0,
		$lat_max =   90.0,
		$lng_min = -180.0,
		$lng_max =  180.0
	) {

		global $wpdb;

		$query = <<<QUERY

SELECT
	sites.id,
	sites.label,
	sites.street,
	sites.city,
	sites.state,
	sites.zip,
	sites.lat,
	sites.lng,
	1 AS assigned,
	1 AS today,
	1 AS urgent
	FROM `{$wpdb->prefix}service_map_sites` AS sites
	WHERE sites.status  <> 'DELETED'
	  AND sites.lat BETWEEN %f AND %f
	  AND sites.lng BETWEEN %f AND %f
	ORDER BY `id` ASC
	LIMIT %d, %d;

QUERY;

		$query = $wpdb->prepare(
			$query,
			$lat_min,
			$lat_max,
			$lng_min,
			$lng_max,
			$offset,
			$limit
		);

		$sites = $wpdb->get_results( $query, ARRAY_A );

		foreach( $sites as &$site ) {

			$site['assignments'] = Service_Map_Data::get_site_assignments( $site['id'] );
			$site['schedule']    = Service_Map_Data::get_site_schedule(    $site['id'] );
			$site['notes']       = Service_Map_Data::get_site_notes(       $site['id'] );
			$site['history']     = Service_Map_Data::get_site_history(     $site['id'] );

		}

		return $sites;
	}

	public static function get_site( $site_id ) {
		global $wpdb;

		$query = <<<QUERY

SELECT
	sites.id,
	sites.label,
	sites.street,
	sites.city,
	sites.state,
	sites.zip,
	sites.lat,
	sites.lng
	FROM `{$wpdb->prefix}service_map_sites` AS sites
	WHERE sites.id = {$site_id};

QUERY;

		return $wpdb->get_results( $query, ARRAY_A );
	}

	public static function get_site_assignments( $site_id ) {
		global $wpdb;

		$query = <<<QUERY

SELECT
	assignments.id,
	assignments.assigned_to,
	assignments.assigned_by
	FROM `{$wpdb->prefix}service_map_assignments` AS assignments
	WHERE assignments.site   = {$site_id}
	  AND assignments.status = 'ACTIVE';

QUERY;

		$assignments = $wpdb->get_results( $query, ARRAY_A );

		foreach( $assignments as &$assignment ) {

			$to_name = get_userdata( $assignment['assigned_to'] );
			$assignment['assigned_to_name'] = $to_name->user_nicename;

			$by_name = get_userdata( $assignment['assigned_by'] );
			$assignment['assigned_by_name'] = $by_name->user_nicename;

		}

		return $assignments;
	}

	public static function get_site_schedule( $site_id ) {
		global $wpdb;

		$query = <<<QUERY

SELECT
	events.id,
	events.event,
	events.author
	FROM `{$wpdb->prefix}service_map_events` AS events
	WHERE events.site = {$site_id};

QUERY;

		$events = $wpdb->get_results( $query, ARRAY_A );

		foreach( $events as &$event ) {

			$author = get_userdata( $event['author'] );
			$event['author'] = $author->user_nicename;

		}

		return $events;
	}

	public static function get_site_notes( $site_id ) {
		global $wpdb;

		$query = <<<QUERY

SELECT
	notes.id,
	notes.note,
	notes.author
	FROM `{$wpdb->prefix}service_map_notes` AS notes
	WHERE notes.site   = {$site_id}
	  AND notes.status = 'ACTIVE';

QUERY;

		return $wpdb->get_results( $query, ARRAY_A );
	}

	public static function get_site_history( $site_id ) {
		global $wpdb;

		$query = <<<QUERY

SELECT
	events.id,
	events.event,
	events.author
	FROM `{$wpdb->prefix}service_map_events` AS events
	WHERE events.site = {$site_id};

QUERY;

		$events = $wpdb->get_results( $query, ARRAY_A );

		foreach( $events as &$event ) {

			$author = get_userdata( $event['author'] );
			$event['author'] = $author->user_nicename;

		}

		return $events;
	}

	public static function do_save_item( $site ) {
		global $wpdb;

		if( empty( $site['lat'] ) || empty( $site['lng'] ) ) {
			$site['lat'] = 0.0;
			$site['lng'] = 0.0;
		}

		/* if id > 0, edit existing site */
		if( $site['id'] ) {

			$query = <<<QUERY

UPDATE {$wpdb->prefix}service_map_sites
	SET `label` = '%s',
		`street` = '%s',
		`city` = '%s',
		`state` = '%s',
		`zip` = '%s',
		`lat` = '%s',
		`lng` = '%s'
	WHERE `id` = '%s';

QUERY;

			$query = $wpdb->prepare(
				$query,
				$site['label'],
				$site['street'],
				$site['city'],
				$site['state'],
				$site['zip'],
				$site['lat'],
				$site['lng'],
				$site['id']
			);

			$success = $wpdb->query( $query );

		} else { /* add site */

			$query = <<<QUERY

INSERT INTO {$wpdb->prefix}service_map_sites
	(
		`label`,
		`street`,
		`city`,
		`state`,
		`zip`,
		`lat`,
		`lng`
	)
	VALUES(
		'%s',
		'%s',
		'%s',
		'%s',
		'%s',
		'%s',
		'%s'
	);

QUERY;

			$query = $wpdb->prepare(
				$query,
				$site['label'],
				$site['street'],
				$site['city'],
				$site['state'],
				$site['zip'],
				$site['lat'],
				$site['lng'],
				$site['id']
			);

			$success = $wpdb->query( $query );

		}

	}

	public static function do_delete_item( $site_id ) {
		global $wpdb;

		$query = <<<QUERY

UPDATE {$wpdb->prefix}service_map_sites
	SET `status` = 'DELETED'
	WHERE `id` = '{$site_id}';

QUERY;

		$wpdb->query( $query );

	}

	public static function geocode( $address ) {
		$address = urlencode( $address );

		$url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address={$address}";

		$resp_json = file_get_contents( $url );

		$resp = json_decode ( $resp_json, true );

		if ( $resp['status'] == 'OK' ) {
			$lat = $resp['results'][0]['geometry']['location']['lat'];
			$lng = $resp['results'][0]['geometry']['location']['lng'];
			$formatted_address = $resp['results'][0]['formatted_address'];

			if ( $lat && $lng && $formatted_address ) {
				$data_arr = array();

				array_push(
					$data_arr,
					$lat,
					$lng,
					$formatted_address
				);

				return $data_arr;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

}

/* EOF */
