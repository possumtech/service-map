<?php

/* Can this be stuck in the class? */

add_action( 'wp_ajax_service_map_get_sites', array( 'Service_Map_Display', 'get_sites' ) );

class Service_Map_Display {

	public static function get_sites() {

		if( !is_numeric( $_POST['zoom'] ) ) {
			wp_die( 'zoom not an integer' );
		}

		$bounds = array();

		$bounds['lat']['ne'] = floatval( $_POST['bounds']['ne_lat'] );
		$bounds['lng']['ne'] = floatval( $_POST['bounds']['ne_lng'] );
		$bounds['lat']['sw'] = floatval( $_POST['bounds']['sw_lat'] );
		$bounds['lng']['sw'] = floatval( $_POST['bounds']['sw_lng'] );

		foreach( $bounds['lat'] as $latitude ) {
			if( !Service_Map::validate_latitude( $latitude ) ) {
				wp_die( 'invalid latitude' );
			}
		}

		foreach( $bounds['lng'] as $longitude ) {
			if( !Service_Map::validate_longitude( $longitude ) ) {
				wp_die( 'invalid longitude' );
			}
		}

		$lat_min = MIN( $bounds['lat']['ne'], $bounds['lat']['sw'] );
		$lat_max = MAX( $bounds['lat']['ne'], $bounds['lat']['sw'] );
		$lng_min = MIN( $bounds['lng']['ne'], $bounds['lng']['sw'] );
		$lng_max = MAX( $bounds['lng']['ne'], $bounds['lng']['sw'] );

		$settings = get_option( 'service_map_settings' );
		WPDBG::dump( $settings );
		$settings['lat'] = ( $lat_min + $lat_max ) / 2.0;
		$settings['lng'] = ( $lng_min + $lng_max ) / 2.0;
		$settings['zoom'] = $_POST['zoom'];
		update_option( 'service_map_settings', $settings );

		$results = Service_Map_Data::get_sites(
			0,
			99,
			$lat_min,
			$lat_max,
			$lng_min,
			$lng_max
		);

		echo wp_json_encode( $results );

		wp_die();

	}

}

/* EOF */
