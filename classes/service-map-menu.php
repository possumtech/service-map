<?php

class Service_Map_Menu {

	private $settings;

	public function __construct( $settings ) {
		$this->settings = $settings;

		add_action( 'admin_menu', array( &$this, 'do_menu' ) );
		add_action( 'admin_init', array( &$this, 'do_menu_init' ) );

		add_action( 'admin_enqueue_scripts', array( &$this, 'do_scripts' ) );

	}

	public function do_menu() {

		add_menu_page(
			'Service Map',
			'Service Map',
			'manage_options',
			'service-map',
			array( &$this, 'do_menu_page' ),
			plugin_dir_url( __FILE__ ) . '../graphics/service-map.png',
			2.5
		);

		add_submenu_page(
			'service-map',
			'Service Map Settings',
			'Settings',
			'manage_options',
			'service-map-settings',
			array( &$this, 'do_menu_page_settings' )
		);

	}

	public function do_menu_init() {

		register_setting( 'service_map_settings', 'service_map_settings' );

	}

	public function do_scripts( $hook ) {

		if( !preg_match( '/toplevel_page_service-map/', $hook ) ) {
			return;
		}

		wp_enqueue_style(
			'service-map',
			plugin_dir_url( __FILE__ ) . '../styles/service-map.css'
		);

		wp_enqueue_script(
			'service-map',
			plugin_dir_url( __FILE__ ) . '../scripts/service-map.js',
			array( 'jquery' )
		);

		wp_enqueue_script(
			'service-map_table',
			plugin_dir_url( __FILE__ ) . '../scripts/service-map-table.js',
			array( 'jquery' )
		);

		wp_enqueue_script(
			'service-map_search',
			plugin_dir_url( __FILE__ ) . '../scripts/service-map-search.js',
			array( 'jquery' )
		);

		wp_enqueue_script(
			'service-map_google',
			'https://maps.googleapis.com/maps/api/js?key=' . $this->settings['key']
		);

		$service_map_settings = array(

				'lat' => $this->settings['lat'],
				'lng' => $this->settings['lng'],
				'zoom' => $this->settings['zoom'],

		);

		$service_map_table  = array();
		$service_map_search = array();

		wp_localize_script(
			'service-map',
			'service_map',
			array(
				'settings' => $service_map_settings,
				'table'    => $service_map_table,
				'search'   => $service_map_search,
			)
		);

		add_screen_option(
			'per_page',
			array(
				'label'   => 'Sites',
				'default' => 10,
				'option'  => 'sites_per_page'
			)
		);

	}

	public static function do_menu_page() {

		$plugin_dir = plugin_dir_path( __FILE__ );
		$plugin_dir = preg_replace( '`/classes`', '', $plugin_dir );
		require_once(  "{$plugin_dir}/snippets/service-map_page.php" );

	}

	public static function do_menu_page_search() {
		echo 'SEARCH';
	}

	public static function do_menu_page_table() {

		$service_map_table = new Service_Map_Table();

		$service_map_table->prepare_items();

		$service_map_table->display();

	}

	public function do_menu_page_settings() {

		$options = array(
			'key'  => 'Google API Key',
			'lat'  => 'Latitude',
			'lng'  => 'Longitude',
			'zoom' => 'Zoom Level',
		);

?>
<div class="wrap">
	<h2>Service Map</h2>

	<form method="post" action="options.php">
		<?php settings_fields( 'service_map_settings' ); ?>
		<?php do_settings_sections( 'service_map_settings' ); ?>
		<table class="form-table">

		<?php foreach( $options as $option => $label ) { ?>

			<tr valign="top">
			<th scope="row"><?php echo $label; ?></th>
				<td>
					<input
						type="text"
						name="service_map_settings[<?php echo $option ?>]"
						value="<?php echo $this->settings[$option]; ?>"
						/>
				</td>
			</tr>

		<?php } ?>

		</table>

		<?php submit_button(); ?>

	</form>
</div>

<?php

	}

}

/* EOF */
