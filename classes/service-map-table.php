<?php

if( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/* Can these be stuck in the class? */

add_action(
	'wp_ajax_service_map_get_site',
	array( 'Service_Map_Table', 'do_get_site' )
);

add_action(
	'wp_ajax_service_map_add_edit_item',
	array( 'Service_Map_Table', 'do_add_edit_item' )
);

add_action(
	'wp_ajax_service_map_delete_item',
	array( 'Service_Map_Table', 'do_delete_item' )
);

class Service_Map_Table extends WP_List_Table {
	public $found_data = array();

	private $sites = array();

	public function __construct() {

		parent::__construct( array(
			'singular' => 'site',
			'plural'   => 'sites',
			'ajax'     => true
		));

	}

	public static function do_get_site() {

		WPDBG::dump( $_GET );

		$site = Service_Map_Data::get_site( $_GET['id'] );

		$site = json_encode( $site );

		echo $site;

		wp_die();
	}

	public static function do_add_edit_item() {

		Service_Map_Data::do_save_item( $_GET['site'] );

		self::do_table_refresh();

		wp_die();
	}

	public static function do_delete_item() {

		Service_Map_Data::do_delete_item( $_GET['id'] );

		self::do_table_refresh();

	}

	public static function do_table_refresh() {

		$GLOBALS['hook_suffix'] = 'service-map_table';
		$new_table = new Service_Map_Table();

		$new_table->prepare_items();

		ob_start();

		$new_table->display();

		$display = ob_get_clean();

		$return = array(
			'status' => 'success',
			'table'  => $display
		);

		$return = json_encode( $return );

		echo $return;

		wp_die();
	}

	public function display() {

		$dir = plugin_dir_path( __FILE__ );
		include "{$dir}../snippets/service-map_table_add-edit.php";

		parent::display();

	}

	public function no_items() {
		echo 'No sites found';
	}

	public function column_default( $item, $column_name ) {
		return $item[$column_name];
	}

	public function get_columns() {

		$columns = array(
			'cb' => '<input type="checkbox"/>',
			'label' => 'Site',
			'street' => 'Street',
			'city' => 'City',
			'state' => 'State',
			'zip' => 'zip',
		);

		return $columns;
	}

	public function get_sortable_columns() {

		$sortable_columns = array(
			'label' => array( 'label', false ),
			'street' => array( 'street', false ),
			'city' => array( 'city', false ),
			'state' => array( 'state', false ),
			'zip' => array( 'zip', false),
		);

		return $sortable_columns;
	}

	public function usort_reorder( $left, $right ) {

		$orderby = ( !empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'label';
		$order = ( !empty( $_GET['order'] ) ) ? $_GET['order'] : 'asc';
		$result = strcmp( $left[$orderby], $right[$orderby] );

		return ( $order === 'asc' ) ? $result : -$result;
	}

	public function column_label( $item ) {

		$hyperlink  = "<a href='#' ";
		$hyperlink .= "id='service-map_table_%s_%s' ";
		$hyperlink .= "class='service-map_table_%s' >%s</a>";

		$actions = array(

			'edit' => sprintf(
				$hyperlink,
				'edit',
				$item['id'],
				'edit',
				'Edit'
			),

			'delete' => sprintf(
				$hyperlink,
				'delete',
				$item['id'],
				'delete',
				'Delete'
			),

		);

		return sprintf(
			'%1$s %2$s',
			$item['label'],
			$this->row_actions( $actions )
		);

	}

	public function column_cb( $item ) {

		return sprintf(
			'<input type="checkbox" name="site[]" value="%s"/>',
			$item['id']
		);

	}

	public function prepare_items() {

		$this->sites = Service_Map_Data::get_sites( 0, 20 );

		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array( $columns, $hidden, $sortable );
		usort( $this->sites, array( &$this, 'usort_reorder' ) );

		$per_page = 5;
		$current_page = $this->get_pagenum();
		$total_items = count( $this->sites );

		$this->found_data = array_slice(
			$this->sites,
			( ( $current_page - 1 ) * $per_page ),
			$per_page
		);

		$this->set_pagination_args( array(
			'total_items'=>$total_items,
			'per_page'=>$per_page
		) );

		$this->items = $this->found_data;
	}

}

/* EOF */
