<div id="service-map" class="wrap">

		<h2>
			Service Map
			<a href="#" class="add-new-h2">Add New Site</a>
		</h2>

	<div id="service-map_top">

		<div id="service-map_canvas"></div>

		<div id="service-map_details">

			<div id="service-map_details_title">
				<h2 class="service-map_details_data"></h2>
			</div>

			<div id="service-map_details_assignments">
				<span class="service-map_details_data"></span>
			</div>

			<div id="service-map_details_schedule">
				<span class="service-map_details_data"></span>
			</div>

			<div id="service-map_details_notes">
				<span class="service-map_details_data"></span>
			</div>

			<div id="service-map_details_history">
				<span class="service-map_details_data"></span>
			</div>

		</div>

	</div>

	<div id="service-map_search">

		<?php Service_Map_Menu::do_menu_page_search(); ?>

	</div>

	<div id="service-map_table">

		<?php Service_Map_Menu::do_menu_page_table(); ?>

	</div>

</div>
