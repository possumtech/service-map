<div id="service-map_table_add-edit">

	<table class="form-table">

		<tr class="form-field service-map_table_right">

			<th scope="row">

				<label for="service-map_table[label]">
					Site
				</label>

			</th>

			<td colspan="2">

				<input
					name="service-map_table[label]"
					id="service-map_table[label]"
					type="text"
					/>

			</td>

		</tr>

		<tr class="form-field service-map_table_right">

			<th scope="row">

				<label for="service-map_table[street]">
					Street
				</label>

			</th>

			<td colspan="5">

				<input
					name="service-map_table[id]"
					id="service-map_table[id]"
					type="hidden"
					value="0"
					/>

				<input
					name="service-map_table[street]"
					id="service-map_table[street]"
					type="text"
					/>

			</td>

		</tr>
		<tr class="form-field service-map_table_center">

			<th scope="city">
				<label for="service-map_table[city]">
					City
				</label>
			</th>

			<th scope="state">
				<label for="service-map_table[state]">
					State
				</label>
			</th>

			<th scope="zip">
				<label for="service-map_table[zip]">
					Zip
				</label>
			</th>

			<th scope="lat">
				<label for="service-map_table[lat]">
					Latitude
				</label>
			</th>

			<th scope="lng">
				<label for="service-map_table[lng]">
					Longitude
				</label>
			</th>

			<th>
			</th>

		</tr>

		<tr class="form-field">

			<td>
				<input
					name="service-map_table[city]"
					id="service-map_table[city]"
					type="text"
					/>
			</td>

			<td>


				<select
					name="service-map_table[state]"
					id="service-map_table[state]"
					>

					<option value=""></option>
					<option value="AL">Alabama</option>
					<option value="AK">Alaska</option>
					<option value="AZ">Arizona</option>
					<option value="AR">Arkansas</option>
					<option value="CA">California</option>
					<option value="CO">Colorado</option>
					<option value="CT">Connecticut</option>
					<option value="DE">Delaware</option>
					<option value="DC">District Of Columbia</option>
					<option value="FL">Florida</option>
					<option value="GA">Georgia</option>
					<option value="HI">Hawaii</option>
					<option value="ID">Idaho</option>
					<option value="IL">Illinois</option>
					<option value="IN">Indiana</option>
					<option value="IA">Iowa</option>
					<option value="KS">Kansas</option>
					<option value="KY">Kentucky</option>
					<option value="LA">Louisiana</option>
					<option value="ME">Maine</option>
					<option value="MD">Maryland</option>
					<option value="MA">Massachusetts</option>
					<option value="MI">Michigan</option>
					<option value="MN">Minnesota</option>
					<option value="MS">Mississippi</option>
					<option value="MO">Missouri</option>
					<option value="MT">Montana</option>
					<option value="NE">Nebraska</option>
					<option value="NV">Nevada</option>
					<option value="NH">New Hampshire</option>
					<option value="NJ">New Jersey</option>
					<option value="NM">New Mexico</option>
					<option value="NY">New York</option>
					<option value="NC">North Carolina</option>
					<option value="ND">North Dakota</option>
					<option value="OH">Ohio</option>
					<option value="OK">Oklahoma</option>
					<option value="OR">Oregon</option>
					<option value="PA">Pennsylvania</option>
					<option value="RI">Rhode Island</option>
					<option value="SC">South Carolina</option>
					<option value="SD">South Dakota</option>
					<option value="TN">Tennessee</option>
					<option value="TX">Texas</option>
					<option value="UT">Utah</option>
					<option value="VT">Vermont</option>
					<option value="VA">Virginia</option>
					<option value="WA">Washington</option>
					<option value="WV">West Virginia</option>
					<option value="WI">Wisconsin</option>
					<option value="WY">Wyoming</option>

				</select>

			</td>

			<td>
				<input
					name="service-map_table[zip]"
					id="service-map_table[zip]"
					type="text"
					/>
			</td>

			<td>
				<input
					name="service-map_table[lat]"
					id="service-map_table[lat]"
					type="text"
					/>
			</td>

			<td>
				<input
					name="service-map_table[lng]"
					id="service-map_table[lng]"
					type="text"
					/>
			</td>

			<td>
				<input
					class="button button-secondary button-large"
					name="service-map_table[geocode]"
					id="service-map_table_add-edit_geocode"
					type="button"
					value="Geocode"
					/>
			</td>

		</tr>

		<tr class="service-map_table_center">

			<th>

				<input
					class="button button-secondary button-large"
					type="button"
					id="service-map_table_cancel"
					value="Cancel"
					/>

			</td>

		<th colspan="5">

				<input
					class="button button-primary button-large"
					type="button"
					id="service-map_table_save"
					value="Save Changes"
					/>

			</td>

		</tr>

	</table>

</div>
