<?php
/*
 * Plugin Name: Service Map
 * Plugin URI: http://www.github.com/wikitopian/service-map
 * Description: Service Map displays an interactive map of locations
 * Author: @wikitopian
 * Version: 0.1
 * License: GPLv3
 * Author URI: http://www.github.com/wikitopian
 */

require_once( '/home/parrott/repo/wpdbg/wpdbg.php' );

require_once( 'classes/service-map-data.php' );
require_once( 'classes/service-map-menu.php' );
require_once( 'classes/service-map-display.php' );
require_once( 'classes/service-map-table.php' );
require_once( 'classes/service-map-search.php' );

class Service_Map {

	private $settings;

	private $menu;

	public function __construct() {

		$settings_default = array(
			'key' => '',
			'lat' => 39.1000,
			'lng' => -84.5167,
			'zoom' => 4
		);

		$this->settings = get_option( 'service_map_settings', $settings_default );

		WPDBG::dump( $this->settings );
		$this->menu = new Service_Map_Menu( $this->settings );

	}

	public static function validate_latitude( $latitude ) {

		if( !is_float( $latitude ) ) {
			return( false );
		}

		if( $latitude > 90.0 || $latitude < -90.0 ) {
			return( false );
		}

		return( true );

	}

	public static function validate_longitude( $longitude ) {

		if( !is_float( $longitude ) ) {
			return( false );
		}

		if( $longitude > 180.0 || $longitude < -180.0 ) {
			return( false );
		}

		return( true );

	}



}

register_activation_hook(
	__FILE__,
	array( 'Service_Map_Data', 'do_tables' )
);

$service_map = new Service_Map();

/* EOF */
