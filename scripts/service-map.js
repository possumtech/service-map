var map;
var infoWindow;
var markers = [];

jQuery( document ).ready( function( $ ) {
	service_map.service_map( $ );
});


service_map.service_map = function( $ ) {

	if( navigator.geolocation ) {

		navigator.geolocation.getCurrentPosition(
			getPosition,
			getPositionError
		);

	} else {
		getPositionError();
	}

	function getPosition( position ) {

		doMap(
			position.coords.latitude,
			position.coords.longitude
		);

	}

	function getPositionError() {

		doMap(
			service_map['settings']['lat'],
			service_map['settings']['lng']
		);

	}

	function doMap( lat, lng ) {

		var mapCoord = new google.maps.LatLng( lat, lng );

		var mapOptions = {
			zoom: parseInt( service_map['settings']['zoom'] ),
			center: mapCoord
		};

		map = new google.maps.Map(
				document.getElementById( 'service-map_canvas' ),
				mapOptions
		);

		infoWindow = new google.maps.InfoWindow();

		google.maps.event.addListener(map, 'idle', function(ev){
			getSites();
		});

	}

	function getSites() {

		var bounds = map.getBounds();
		var ne = bounds.getNorthEast();
		var sw = bounds.getSouthWest();

		var corners = {
			'ne_lat': ne.lat(),
			'ne_lng': ne.lng(),
			'sw_lat': sw.lat(),
			'sw_lng': sw.lng(),
		};

		var data = {
			'action': 'service_map_get_sites',
			'bounds': corners,
			'zoom': map.getZoom()
		};

		$.post( ajaxurl, data, function( sites ) {

			sites = $.parseJSON( sites );

			for( site in sites ) {

				sites[site]['label'] = sites[site]['label'].replace( "&apos;", "'" );

				doSite(
						sites[site]['label'],
						sites[site]['lat'],
						sites[site]['lng'],
						sites[site]['today'],
						sites[site]['urgent'],
						sites[site]['assignments'],
						sites[site]['schedule'],
						sites[site]['notes'],
						sites[site]['history']
					  );

			}

		});

	}

	function doSite(
			label,
			lat,
			lng,
			today,
			urgent,
			assignments,
			schedule,
			notes,
			history
			) {

		var marker = new google.maps.Marker({
			map: map,
			position: new google.maps.LatLng( lat, lng ),
			icon: doMarker( today, urgent, true, true )
		});

		google.maps.event.addListener(marker, 'click', function() {

			var edit = $( '#service-map_edit' );

			edit.title       = $( '#service-map_details_title       .service-map_details_data' );
			edit.assignments = $( '#service-map_details_assignments .service-map_details_data' );
			edit.schedule    = $( '#service-map_details_schedule    .service-map_details_data' );
			edit.notes       = $( '#service-map_details_notes       .service-map_details_data' );
			edit.history     = $( '#service-map_details_history     .service-map_details_data' );

			edit.title.text(    label );

			/* assignments */
			var assignment_text = '';
			for( assignment_key in assignments ) {

				assignment_text += 'TO: ' + assignments[assignment_key]['assigned_to_name'];
				assignment_text += 'BY: ' + assignments[assignment_key]['assigned_by_name'];

			}
			edit.assignments.text( assignment_text );

			/* schedule */
			var schedule_text = '';
			for( schedule_key in schedule ) {

				schedule_text += schedule[schedule_key]['event'] + '::' + schedule[schedule_key]['author'];

			}
			edit.schedule.text( schedule_text );

			/* notes */
			var note_text = '';
			for( note_key in notes ) {

				note_text += notes[note_key]['note'];

			}
			edit.notes.text( note_text );

			/* history */
			var history_text = '';
			for( history_key in history ) {

				history_text += history[history_key]['event'] + '::' + history[history_key]['author'];

			}
			edit.history.text( history_text );

		});

		markers.push(marker);

	}

	function doMarker( is_today, is_urgent, is_assigned, has_notes ) {

		var icon_url = 'https://chart.googleapis.com/chart?chst={pin_type}&chld={pin_star}|{glyph}|{fg_color}|{bg_color}';

		icon_url = icon_url.replace( '{pin_type}', 'd_map_xpin_icon' );

		if( is_assigned ) {
			icon_url = icon_url.replace( '{pin_star}', 'pin_star' );
		} else {
			icon_url = icon_url.replace( '{pin_star}', 'pin' );
		}

		if( is_assigned && has_notes ) {
			icon_url = icon_url.replace( '{glyph}', 'glyphish_chat2' );
		} else if( assigned ) {
			icon_url = icon_url.replace( '{glyph}', 'glyphish_target' );
		} else if( is_urgent || is_today ) {
			icon_url = icon_url.replace( '{glyph}', 'glyphish_stopwatch' );
		} else {
			icon_url = icon_url.replace( '{glyph}', 'glyphish_todo' );
		}

		if( is_assigned && is_urgent ) {
			icon_url = icon_url.replace( '{fg_color}', 'FF0000' );
			icon_url = icon_url.replace( '{bg_color}', 'FF0000' );
		} else if( ( is_assigned && is_today ) || ( !is_assigned && is_urgent ) ) {
			icon_url = icon_url.replace( '{fg_color}', 'FFA500' );
			icon_url = icon_url.replace( '{bg_color}', 'FFA500' );
		} else {
			icon_url = icon_url.replace( '{fg_color}', '008000' );
			icon_url = icon_url.replace( '{bg_color}', '008000' );
		}

		return icon_url;

	}

}
