jQuery( document ).ready( function( $ ) {
	service_map.service_map_table( $ );
});

service_map.service_map_table = function( $ ) {

	$( 'a.add-new_h2' ).click( function(){
		form_add_new();
	});

	$( '#service-map_table_geocode' ).click( function() {
		geocode( fetch_id() );
	});

	$( '#service-map_table_save' ).click( function() {
		form_save( fetch_id() );
	});

	$( '#service-map_table_cancel' ).click( function() {
		form_clear();
	});

	$( 'a.service-map_table_edit' ).click( function() {

		$( '#service-map_table_add-edit' ).show();

		$( "input[id='service-map_table[geocode]']" ).val( 'Geocode' );

		var id = fetch_id( this );

		$.ajax({
			url: ajaxurl,
			data: {
				action: 'service_map_get_site',
				id: id
			},
			success: function( response ) {

				response = $.parseJSON( response );
				response = response[0];

				for( item in response ) {

					if( item == 'state' ) {
						var field = "select[id='service-map_table[state]']";
						var field_value = response[item];
					} else {
						var field = "input[id='service-map_table["+item+"]']";
						var field_value = response[item].replace( "&apos;", "'" );
					}

					$( field ).val( field_value );


				}

			}

		});

	});

	$( 'a.service-map_table_delete' ).click( function() {

		var id = fetch_id( this );

		$.ajax({
			url: ajaxurl,
			data: {
				action: 'service_map_delete_item',
				id: id
			},
			success: function( response ) {
				form_clear( response );
			}

		});

	});

	var form_add_new = function() {

		form_clear();

		$( '#service-map_table_add-edit' ).show();

		$( "input[id='service-map_table[geocode]']" ).val( 'Geocode' );

	}

	var geocode = function( id ) {

		var geocoder = new google.maps.Geocoder();

		var address =
			  $( "input[id^='service-map_table[street]']" ).val()
			+ ' '
			+ $( "input[id^='service-map_table[city]']" ).val()
			+ ' '
			+ $( "input[id^='service-map_table[state]']" ).val()
			+ ' '
			+ $( "input[id^='service-map_table[zip]']" ).val();

		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {

				var lat = results[0].geometry.location.lat();
				var lng = results[0].geometry.location.lng();

				$( "input[id^='service-map_table[lat]']" ).val(lat);
				$( "input[id^='service-map_table[lng]']" ).val(lng);

			} else {

				alert( 'Geocode Failure!' );

				$( "input[id^='service-map_table[lat]']" ).val(0);
				$( "input[id^='service-map_table[lng]']" ).val(0);

			}

		});

	}

	var form_save = function( id ) {

		var elems = $( "[id^='service-map_table[']" );

		var site = new Object();
		var site_regex = /service-map_table\[(\w+)/;
		$.each( elems, function( key, elem ) {

			var site_name =  $( elem ).attr( 'name' );
			site_name = site_regex.exec( site_name );
			site_name = site_name[1];

			var site_value = $( "[id^='service-map_table[" + site_name + "]']" ).val();
			site_value = site_value.replace( "'", "&apos;" );
			site[site_name] = site_value;

		});

		$.ajax({
			url: ajaxurl,
			data: {
				action: 'service_map_add_edit_item',
				id: id,
				site: site
			},
			success: function( response ) {

				form_clear( response );

			}

		});

	}

	var form_clear = function( response ) {

		$( "[id^='service-map_table[']" ).val( '' );

		$( '#service-map_table_add-edit' ).hide();

		if( response ) {

				$( 'div#service-map_table' ).empty();

				response = $.parseJSON( response );

				table = response['table'];

				$( 'div#service-map_table' ).html( table );

				service_map.service_map( $ );

				service_map.service_map_table( $ );

		}

	}

	var fetch_id = function( obj ) {

		if( obj ) {

			var id = $( obj ).attr( 'id' );

			var id_regex = /service-map_table_\w+_(\d+)/;
			var id = id_regex.exec( id );
			var id = id[1];

			return id;

		} else {

			return $( "input[id='service-map-table[id]']" ).val();

		}

	}

}

